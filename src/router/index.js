import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Main from '../view/index.js'
import Detail from '../view/detail.js'
import '../App.css'
class index extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path='/main' component={Main}></Route>
                    <Route path='/detail' component={Detail}></Route>
                </Switch>
            </BrowserRouter>
        )
    }
}
export default index